#include <stdlib.h>
#include <stdio.h>
#define N 12

static void initia (int iNb_jours[N]) {
  for (int i = 1; i < N + 1; i ++) {
    if (i == 2) {
      iNb_jours[i] = 28;
    }
    else if ((i % 2 == 0 && i <= 7) || (i % 2 != 0 && i > 7)) {
      iNb_jours[i] = 30;
    }
    else {
      iNb_jours[i] = 31;
    }

    }

}
 static void impression (int iNb_jours[N]) {
   for (int i = 1; i < N + 1; i ++) {
     printf("%d \n", iNb_jours[i]);
   }
 }

 void main () {
   int tab[N];
   initia(tab);
   impression(tab);


 }
